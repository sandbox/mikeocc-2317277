<?php

/**
 * @file
 * Class for handling a view that gets its data not from the database, but from
 * a Solr server.
 */

// This module follows the conventions of the views module. There does not appear to be a per-module mechanism to get Coder shut its trap ;)

/**
 * Class for querying the Excellence Gateway Read API by passing a SOLR
 * query to the `resources` method.
 */
class eg_views_query extends views_plugin_query {
  /**
   * Array of parameters for Solr query.
   */
  protected $params;

  /**
   * The array to which query parameters are added as the query is constructed.
   */
  protected $query_params;

  /**
   * Array of all encountered errors. Each of these is fatal, meaning that a
   * non-empty $errors property will result in an empty result being returned.
   */
  protected $errors;

  /**
   * Store results of solr search.
   */
  public $solr_results;

  /**
   * Array of where conditions.
   *
   * Needed for grouping of query conditions.
   */
  protected $where = array();

  /**
   * {@inheritdoc}
   *
   * Assemble the SOLR query parameters in the view->build_info['query'] from
   * this object's params.
   */
  public function build(&$view) {
    $view->init_pager();

    // Let the pager modify the query to add limits (in fact, we just want it
    // to parse querystring parameters since we handle the paging parameters
    // later in this method).
    $this->pager->query();

    // Set aliases of the fields.
    foreach ($view->field as $field_name => &$field) {
      $field->field_alias = $field_name;
    }

    // Add fields to the query so they will be shown in solr document.
    $this->params['fl'] = array_keys($view->field);

    $query_params = array();
    if (isset($this->params['q'])) {
      $query_params['q'] = $this->params['q'];
    }
    else {
      $query_params['q'] = '';
    }

    $query_params['rows'] = $this->pager->options['items_per_page'];
    $query_params['start'] = $this->pager->current_page * $this->pager->options['items_per_page'];

    // If we display all items without pager.
    if ($query_params['rows'] == 0) {
      $query_params['rows'] = 100000;
    }

    // Add fields, add url if not present.
    if (isset($this->params['fl'])) {
      $query_params['fl'] = $this->params['fl'];
    }
    if (!in_array('url', $query_params['fl'])) {
      $query_params['fl'][] = 'url';
    }

    $query_params['fl'] = implode(',', $query_params['fl']);

    $where = $this->where;

    // Remove any empty conditions (exposed filters), they will cause an error.
    foreach ($where as &$where_condition) {
      foreach ($where_condition['conditions'] as $index => $condition) {
        if ($condition['value'] == '') {
          unset($where_condition['conditions'][$index]);
        }
      }
    }
    // Add conditions to filter parameter.
    $conditions = array('conditions' => $where, 'type' => 'AND');
    $conditions_string = $this->build_where_string($conditions);

    if (!empty($conditions_string)) {
      $query_params['fq'][] = $conditions_string;
    }

    // Set query type if it is present.
    if (isset($this->params['defType'])) {
      $query_params['defType'] = $this->params['defType'];
    }

    // Set sort order.
    if (isset($this->params['sort'])) {
      $query_params['sort'] = $this->params['sort'][0] . " " . $this->params['sort'][1];
    }

    // Faceted queries from exposed filters get added at the end as separate fq
    // parameters so that we can added localparam information to exlude those
    // filters from the facet counts.

    // List of all exposed filters.
    $exposed_filters = array();

    foreach ($view->filter as $filter) {
      if ($filter instanceof eg_views_handler_filter_facet && $filter->is_exposed()) {
        $exposed_filters[] = $filter->real_field;
      }
    }
    $excluded_filters = array_merge(array(), $exposed_filters);

    foreach ($this->facets as $name => $values) {
      if ($values) {
        $val_str = '';
        foreach ($values as $value) {
          if ($value) {
            $value = trim($value);

            if (drupal_strlen($value) > 0) {
              if (strlen($val_str) > 0) {
                $val_str .= ' AND ';
              }
              $val_str .= "\"{$value}\"";
            }
          }
        }
        if (drupal_strlen($val_str) > 0) {
          $query_params['fq'][] = "{!tag=" . $name . "}" . $name . ":(" . $val_str . ")";

          // Remove facet query from filters to be excluded.
          $idx = array_search($name, $excluded_filters);
          if ($idx !== FALSE) {
            unset($excluded_filters[$idx]);
          }
        }
      }
    }

    foreach ($exposed_filters as $filter_name) {
      $excluded = implode(',', array_filter($excluded_filters, function($elm) use($filter_name) {
        return $elm !== $filter_name;
      }));
      if (strlen($excluded) > 0) {
        $query_params['facet.field'][] = "{!ex=" . $excluded . "}" . $filter_name;
      }
      else {
        $query_params['facet.field'][] = $filter_name;
      }
    }
    if (isset($query_params['facet.field'])) {
      $query_params['facet'] = "true";
      $query_params['facet.mincount'] = "1";
      $query_params['facet.sort'] = "count";
    }

    $this->query_params = $query_params;

    // Export parameters for preview.
    $view->build_info['query'] = var_export($query_params, TRUE);
  }

  /**
   * Let modules modify the query just prior to finalizing it.
   */
  public function alter(&$view) {
    foreach (module_implements('views_query_alter') as $module) {
      $function = $module . '_views_query_alter';
      $function($view, $this);
    }
  }

  /**
   * Executes the query and fills the view object with according values.
   *
   * Values to set: $view->result, $view->total_rows, $view->execute_time,
   * $view->pager['current_page'].
   */
  public function execute(&$view) {
    try {
      $start = microtime(TRUE);
      $result = &drupal_static(__FUNCTION__);

      if (!isset($result)) {
        // Execute the search.
        $result = eg_get_resources_solr($this->query_params);
      }
      // Store results, just the documents for Views but keep the other details
      // internally.
      $view->result = $result->response->response->docs;
      $this->solr_results = $result->response;

      // Store the results.
      $this->pager->total_items = $view->total_rows = $result->response->response->numFound;
      $this->pager->update_page_info();

      // We shouldn't use $results['performance']['complete'] here, since
      // extracting the results probably takes considerable time as well.
      $view->execute_time = microtime(TRUE) - $start;
    }
    catch (Exception $e) {
      $this->errors[] = $e->getMessage();
    }

    if ($this->errors) {
      foreach ($this->errors as $msg) {
        drupal_set_message(check_plain($msg), 'error');
      }
      $view->result = array();
      $view->total_rows = 0;
      $view->execute_time = 0;
      return;
    }
  }


  /**
   * Used by eg_views_handler_argument::query() to set query parameters.
   *
   * @param string $type
   *   The index field to search.
   *
   * @param string $value
   *   The value to match the field.
   *
   * @param bool $exclude
   *   A flag indicating if matches should be excluded from the results.
   */
  public function add_filter($type, $value, $exclude = FALSE) {
    $exclude_string = ($exclude) ? '-' : '';
    $this->params['filters'][] = $exclude_string . $type . ':(' . $value . ')';
  }

  /**
   * Used by handlers to add sorting to the results.
   *
   * @param string $field
   *   The name of the index field to sort on.
   *
   * @param string $order
   *   The order to to use.
   */
  public function add_sort($field, $order) {
    $this->params['sort'] = array($field, $order);
  }

  /**
   * Used to add parameters to the search parameters.
   *
   * @param string $key
   *   The parameter key to add, typically this will be 'q'.
   *
   * @param string $value
   *   The value for the parameter.
   */
  public function add_parameter($key, $value) {
    $this->params[$key] = $value;
  }

  /**
   * {@inheritdoc}
   */
  public function add_field($table_alias, $field, $alias = '', $params = array()) {
    if (isset($table_alias[$field])) {
      return $table_alias[$field];
    }
  }

  /**
   * Used to access the search parameters.
   *
   * @return array
   *   Keyed array of parameters.
   */
  public function get_params() {
    return $this->params;
  }

  /**
   * Build filter string from where array.
   */
  function build_where_string($where) {
    if (!isset($where['conditions'])) {
      return $where['field'] . ':(' . $where['value'] . ')';
    }
    else {
      $condition_strings = array();
      foreach ($where['conditions'] as $condition) {
        $condition_strings[] = $this->build_where_string($condition);
      }
      $condition_strings = array_filter($condition_strings);
      return implode(' ' . $where['type'] . ' ', $condition_strings);
    }
  }

  /**
   * Support for grouping.
   */
  function add_where($group, $field, $value = NULL, $operator = NULL) {
    // Ensure all variants of 0 are actually 0. Thus '', 0 and NULL are all
    // the default group.
    if (empty($group)) {
      $group = 0;
    }

    // Check for a group.
    if (!isset($this->where[$group])) {
      $this->set_where_group('AND', $group);
    }

    $this->where[$group]['conditions'][] = array(
      'field' => $field,
      'value' => $value,
      'operator' => $operator,
    );
  }

  /**
   * Support for groupping.
   */
  function set_where_group($type = 'AND', $group = NULL, $where = 'where') {
    // Set an alias.
    $groups = &$this->$where;

    if (!isset($group)) {
      $group = empty($groups) ? 1 : max(array_keys($groups)) + 1;
    }

    // Create an empty group.
    if (empty($groups[$group])) {
      $groups[$group] = array('conditions' => array(), 'args' => array());
    }
    $groups[$group]['type'] = drupal_strtoupper($type);
    return $group;
  }

  /**
   * Array of facets.
   */
  protected $facets = array();

  /**
   * Retrieve the value for the named facet.
   *
   * @return string
   *   The value of the named facet.
   */
  function facet_value($facet_id) {
    return $facets[$facet_id];
  }

  /**
   * Set the value of a facet.
   */
  function set_facet($facet_id, $facet_value) {
    $this->facets[$facet_id][] = $facet_value;
  }
}
