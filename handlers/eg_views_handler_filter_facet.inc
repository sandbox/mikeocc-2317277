<?php

/**
 * @file
 * Filter handler for search facets.
 */

/**
 * Class for filtering facet searches.
 *
 * @ingroup views_filter_handlers
 */
class eg_views_handler_filter_facet extends views_handler_filter {
  /**
   * Returns the available operators.
   *
   * @return array
   *   An array of operators.
   */
  public function operators() {
    $operators = array(
      'in' => array(
        'title' => t('Is one of'),
        'short' => t('in'),
        'short_single' => t('='),
        'method' => 'op_equal',
        'values' => 1,
      ),
    );
    return $operators;
  }

  /**
   * Returns an array containing a single 'equal to' operator.
   *
   * @return array
   *   An array of operators.
   */
  public function operator_options() {
    return array(
      '=' => t('Is equal to'),
    );
  }

  /**
   * Gets the array of facets relevant to the current query.
   *
   * This array is used when the facet results are exposed in the view as a
   * Filter Criteria. Counts added to the values of the results.
   *
   * @return array
   *   An array of the available facets.
   */
  function get_facet_options_from_results() {
    if (isset($this->view->is_fake) && $this->view->is_fake == TRUE) {
      return array();
    }

    // This is ugly, we want to base the facet listing on the results, but
    // those haven't been generated yet. We can't call execute on the current
    // view since it is already in progress. Instead we clone the view, mark it
    // as a fake to prevent recursion, and execute it to get the results. We
    // can prevent two calls to SOLR by caching responses keyed by SOLR URL.

    $clone_view = $this->view->clone_view();
    $clone_view->is_fake = TRUE;
    $clone_view->execute();

    // Retrieve the facet values stored when the query executed.
    $facet_results = $clone_view->query->solr_results->facet_counts->facet_fields->{$this->real_field};
    $options = array();

    // Awkward processing because the array is alternating string values and
    // integer counts.
    for ($i = 0; $i <= count($facet_results) - 2; $i += 2) {
      $options[$facet_results[$i]] = $facet_results[$i]
        . " (" . $facet_results[$i + 1] . ")";
    }
    $this->value_options = $options;

    return $this->value_options;
  }

  /**
   * Gets the facet values from eg_get_facet_values() and returns them.
   *
   * @return array
   *   An indexed array of Term:Term (count)
   */
  function get_facet_options_all() {
    $this->value_options = eg_get_facet_values($this->real_field);
    return $this->value_options;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $value = $this->value;

    if (is_array($value)) {
      foreach ($value as $key => $val) {
        if ($val) {
          $this->query->set_facet($this->real_field, $val);
        }
      }
    }
    elseif (is_string($value)) {
      $value = trim($value);
      if (strlen($value) > 0) {
        $this->query->set_facet($this->real_field, $value);
      }
    }
  }

  /**
   * Adds the facet values to the exposed form.
   */
  function exposed_form(&$form, &$form_state) {
    $form[$this->options['expose']['identifier']] = array(
      '#type' => 'checkboxes',
      '#options' => $this->get_facet_options_from_results(),
      '#validated' => TRUE,
    );
  }

  /**
   * Updates the user interface with the facet values.
   */
  function value_form(&$form, &$form_state) {
    $options = array();
    $form['value'] = array();
    $default_value = $this->value;;

    $options = array('*' => t('Select all'));
    $options += $this->get_facet_options_all();

    $form['value'] = array(
      '#type' => 'select',
      '#options' => $options,
      '#default_value' => $default_value,
      '#multiple' => TRUE,
    );
  }

  /**
   * Does nothing.
   */
  public function validate() {
  }
}
