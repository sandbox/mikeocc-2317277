<?php

/**
 * @file
 * Format dates emerging from a SOLR index.
 */

/**
 * Date field handler for Solr fields.
 */
class eg_views_handler_field_date extends views_handler_field {
  /**
   * Overrides the views_handler_field::get_value() method.
   *
   * @param object $values
   *   An object containing all retrieved values.
   * @param string $field
   *   Optional name of the field where the value is stored.
   */
  function get_value($values, $field = NULL) {
    $alias = isset($field) ? $this->aliases[$field] : $this->field_alias;

    if (isset($values->{$alias})) {
      if (is_array($values->{$alias})) {
        return implode(" ", $values->{$alias});
      }
      else {
        try {
          $moddate = new DateObject($values->{$alias});
          return date_format_date($moddate, 'medium');
        }
        catch (Exception $e) {
        }
      }
    }
  }

  /**
   * Called to determine what to tell the clicksorter.
   */
  function click_sort($order) {
    $sort_field = (isset($this->definition['click sort field']) ? $this->definition['click sort field'] : $this->real_field);
    $this->query->add_sort($sort_field, $order);
  }
}
