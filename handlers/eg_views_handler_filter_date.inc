<?php

/**
 * @file
 * A filter handler for dates.
 */

/**
 * Class for filtering date searches.
 *
 * @ingroup views_filter_handlers
 */
class eg_views_handler_filter_date extends views_handler_filter_date {
  /**
   * Set up a where clause to select dates between the selected value.
   *
   * @param string $field
   *   Ignored.
   */
  function op_between($field) {
    $value_a_formatted = $this->obtain_formated_date($this->value['min'], '00:00:00');
    $value_b_formatted = $this->obtain_formated_date($this->value['max'], '23:59:59.999');
    $operator = drupal_strtoupper($this->operator);
    $this->query->add_where($this->options['group'],
                            "$this->real_field",
                            '[' . $value_a_formatted . ' TO ' . $value_b_formatted . ']',
                            $operator);
  }

  /**
   * Set a simple date filter.
   *
   * @param string $field
   *   Ignored.
   */
  function op_simple($field) {
    $value = intval(strtotime($this->value['value'], 0));
    $value = new DateObject($value);

    // Probably there is a better way with dateobject, but no way to find how.
    $has_time = strpos($this->value['value'], ':');

    $value_format = $value->format('Y-m-d\TH:i:s\Z');
    switch ($this->operator) {
      case '<':
      case '<=':
        if ($has_time) {
          $value = '[* TO ' . $value_format . ']';
        }
        else {
          $value = '[* TO ' . $value_format . '/DAY+1DAY]';
        }
        break;

      case '>':
      case '>=':
        if ($has_time) {
          $value = '[' . $value_format . ' TO *]';
        }
        else {
          $value = '[' . $value_format . '/DAY TO *]';
        }
        break;

      case '!=':
        if ($has_time) {
          $value = '[* TO ' . $value_format . '-1SECOND] OR [' . $value_format . '+1SECOND TO *]';
        }
        else {
          $value = '[* TO ' . $value_format . '/DAY-1DAY] OR [' . $value_format . '/DAY+1DAY TO *]';
        }
        break;

      case '=':
      default:
        if ($has_time) {
          $value = '[' . $value_format . ' TO ' . $value_format . ']';
        }
        else {
          $value = '[' . $value_format . '/DAY TO ' . $value_format . '/DAY+1DAY]';
        }
        break;

    }
    $this->query->add_where($this->options['group'], "$this->real_field", $value, $this->operator);
  }

  /**
   * Add a type selector to the value form.
   */
  function value_form(&$form, &$form_state) {
    parent::value_form($form, $form_state);

    // Remove offset functionality.
    unset($form['value']['type']);

    // Add class to identify in jquery input fields and add
    // jquery.ui.datepicker.
    if (isset($form['value']['min'])) {
      $form['value']['max']['#attributes']['class'][] = "views_input_date";
      $form['value']['min']['#attributes']['class'][] = "views_input_date";
    }
    else {
      $form['value']['value']['#attributes']['class'][] = "views_input_date";
    }
  }

  /**
   * Overload to allow ranges without end-date, asume * if not present.
   *
   * @param string $input
   *   Ignored
   *
   * @return bool
   *   TRUE if exposed.
   */
  function accept_exposed_input($input) {
    if (empty($this->options['exposed'])) {
      return TRUE;
    }

    // Store this because it will get overwritten.
    $type = $this->value['type'];
    $rc = parent::accept_exposed_input($input);

    $operators = $this->operators();
    if (!empty($this->options['expose']['use_operator']) && !empty($this->options['expose']['operator_id'])) {
      $operator = $input[$this->options['expose']['operator_id']];
    }
    else {
      $operator = $this->operator;
    }

    if ($operators[$operator]['values'] != 1) {
      // This is the line overrided.
      if ($this->value['min'] == '' && $this->value['max'] == '') {
        return FALSE;
      }
      else {
        return TRUE;
      }
    }

    // Restore what got overwritten by the parent.
    $this->value['type'] = $type;
    return $rc;
  }

  /**
   * Obtains a formated data for SOLR.
   *
   * @param date $date_str
   *   date in string format.
   * @param string $round_string
   *   string to add if no time is present
   *
   * @return string
   *   the date formated for SOLR
   */
  function obtain_formated_date($date_str, $round_string = "00:00:00") {
    // Probably there is a better way with dateobject, but no way to find how.
    if ($date_str != '' && !strpos($date_str, ':')) {
      $date_str = $date_str . " " . $round_string;
    }
    $time = intval(strtotime($date_str, 0));
    if ($time != 0) {
      $date = new DateObject($time);
      $date_formatted = $date->format('Y-m-d\TH:i:s\Z');
    }
    else {
      $date_formatted = '*';
    }
    return $date_formatted;
  }
}
