<?php

/**
 * @file
 * Filtering for freetext strings.
 */

/**
 * String filtering for freetext strings.
 *
 * @ingroup views_filter_handlers
 */
class eg_views_handler_filter_freetext extends views_handler_filter_string {
  /**
   * Returns a single equality operator.
   *
   * @return array
   *   An arraoy of operator arrays.
   */
  public function operators() {
    $operators = array(
      'search' => array(
        'title' => t('Contains text'),
        'short' => t('contains'),
        'short_single' => t('C'),
        'method' => 'op_equal',
        'values' => 1,
      ),
    );
    return $operators;
  }

  /**
   * Adds the 'q' parameter to the query parameters.
   *
   * @param string $field
   *   Ignored.
   */
  function op_equal($field) {
    $this->query->add_parameter('q', $this->value);
  }
}
