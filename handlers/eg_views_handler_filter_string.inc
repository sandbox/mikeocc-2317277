<?php

/**
 * @file
 * Filtering for strings.
 */

/**
 * Class for filtering string searches.
 *
 * @ingroup views_filter_handlers
 */
class eg_views_handler_filter_string extends views_handler_filter_string {
  /**
   * Return the list of available operators.
   *
   * This implementation removes the following which are not handled by the
   * search API:
   * -    'shorterthan'
   * -    'longerthan'
   * -    'regular_expression'
   * -    'contains'
   * -    'ends'
   * -    'not_ends'
   *
   * @return array
   *   An array of available operators.
   */
  public function operators() {
    $operators = parent::operators();
    foreach (array('shorterthan', 'longerthan', 'regular_expression',
        'contains', 'ends', 'not_ends') as $key) {
      if (isset($operators[$key])) {
        unset($operators[$key]);
      }
    }
    return $operators;
  }

  /**
   * Adds the 'real_field' to the where clause.
   *
   * @param string $field
   *   Ignored (`real_field` used instead).
   */
  function op_equal($field) {
    if ($this->operator == '!=') {
      $this->real_field = '-' . $this->real_field;
    }
    $this->query->add_where($this->options['group'], $this->real_field, $this->value);
  }

  /**
   * Adds to the 'Where' clause.
   *
   * @param string $field
   *   Ignored (`real_field` used instead).
   */
  function op_word($field) {
    $where_operator = $this->operator == 'word' ? ' OR ' : ' AND ';
    $where = array();

    // Don't filter on empty strings.
    if (empty($this->value)) {
      return;
    }

    preg_match_all('/ (-?)("[^"]+"|[^" ]+)/i', ' ' . $this->value, $matches, PREG_SET_ORDER);
    foreach ($matches as $match) {
      $phrase = FALSE;
      // Strip off phrase quotes.
      if ($match[2]{0} == '"') {
        $match[2] = drupal_substr($match[2], 1, -1);
        $phrase = TRUE;
      }
      $words = trim($match[2], ',?!();:-');
      $words = $phrase ? array($words) : preg_split('/ /', $words, -1, PREG_SPLIT_NO_EMPTY);

      foreach ($words as $word) {
        $where[] = trim($word, " ,!?") . '*';
      }
    }

    if (!$where) {
      return;
    }

    // Previously this was a call_user_func_array but that's unnecessary
    // as views will unpack an array that is a single arg.
    $this->query->add_where($this->options['group'], $this->real_field, implode($where_operator, $where));
  }

  /**
   * Add a clause to match fields with values which start with the given value.
   *
   * @param string $field
   *   Ignored.
   */
  function op_starts($field) {
    $this->query->add_where($this->options['group'],
      $this->real_field, $this->value . '*');
  }

  /**
   * Add a clause to match fields which do not start with the given value.
   *
   * @param string $field
   *   Ignored.
   */
  function op_not_starts($field) {
    $this->query->add_where($this->options['group'],
      '-' . $this->real_field, $this->value . '*');
  }

  /**
   * Add a clause to match fields which do not match with the given value.
   *
   * @param string $field
   *   Ignored.
   */
  function op_not($field) {
    $this->query->add_where($this->options['group'],
      '-' . $this->real_field, $this->value);
  }
}
