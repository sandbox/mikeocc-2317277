<?php

/**
 * @file
 * Fulltext keyword search.
 */

/**
 * Class for fulltext keyword searches.
 *
 * @ingroup views_filter_handlers
 */
class eg_views_keyword_handler_filter extends eg_views_handler_filter {
  /**
   * {@inheritdoc}
   *
   * Places the sort into the search parameters.
   */
  public function query() {
    if (!empty($this->value)) {
      $this->query->add_parameter('q', $this->value);
    }
  }
}
