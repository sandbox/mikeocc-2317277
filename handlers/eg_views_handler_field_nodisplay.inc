<?php

/**
 * @file
 * Handler for SOLR fields which do not have a stored value and so cannot
 * be displayed.
 */

/**
 * Field handler for Solr fields.
 */
class eg_views_handler_field_nodisplay extends views_handler_field {
  /**
   * {@inheritdoc}
   */
  function get_value($values, $field = NULL) {
    return "Not stored";
  }
}
