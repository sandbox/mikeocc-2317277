<?php

/**
 * @file
 * Views argument handler extension.
 */

/**
 * Class that allows searching the site with Apache Solr through a view.
 */
class eg_views_handler_argument extends views_handler_argument {
  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->query->add_filter($this->real_field, $this->argument);
  }
}
