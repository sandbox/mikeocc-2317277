<?php

/**
 * @file
 * Sorting handler for string.
 */

/**
 * Class for sorting for a field.
 */
class eg_views_handler_sort extends views_handler_sort {
  /**
   * {@inheritdoc}
   *
   * Places the sort into the search parameters.
   */
  public function query() {
    $order = drupal_strtolower($this->options['order']);
    $this->query->add_sort($this->real_field, $order);
  }
}
