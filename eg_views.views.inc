<?php

/**
 * @file
 * This module adds a new content type to Views for EG repository content.
 * SOLR fields can be included in the output and as filters.
 * Vocabulary fields will be displayed as faceted filters.
 */

/**
 * Implements hook_views_plugins().
 */
function eg_views_views_plugins() {
  return array(
    'module' => 'eg_views',
    'query' => array(
      'eg_views_query' => array(
        'title' => t('Excellence Gateway Query'),
        'help' => t('Query that allows you to search the EG repository with Apache Solr.'),
        'handler' => 'eg_views_query',
        'parent' => 'views_query',
      ),
    ),
  );
}

/**
 * Implements hook_views_data().
 */
function eg_views_views_data() {
  $table_name = 'eg_repository';

  $data[$table_name]['table']['group'] = t('Excellence Gateway repository');
  $data[$table_name]['table']['base'] = array(
    'query class' => 'eg_views_query',
    'title' => t('Excellence Gateway repository'),
    'help' => t('Searches the Excellence Gateway repository'),
  );

  // We could build a list of fields from the SOLR schema, but for now we'll
  // fix it to the basics.

  $data[$table_name]['url'] = array(
    'title' => 'URL',
    'help' => 'Location of resource',
    'field' => array(
      'handler' => 'eg_views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'eg_views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'eg_views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'eg_views_handler_argument',
    ),
  );

  $data[$table_name]['title'] = array(
    'title' => 'Title',
    'help' => 'Title of resource',
    'field' => array(
      'handler' => 'eg_views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'eg_views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'eg_views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'eg_views_handler_argument',
    ),
  );

  $data[$table_name]['description'] = array(
    'title' => 'Description',
    'help' => 'Short description of resource',
    'field' => array(
      'handler' => 'eg_views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'eg_views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'eg_views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'eg_views_handler_argument',
    ),
  );

  $data[$table_name]['content'] = array(
    'title' => 'All text',
    'help' => 'Full text content of resource (not available for display)',
    'field' => array(
      'handler' => 'eg_views_handler_field_nodisplay',
    ),
    'filter' => array(
      'handler' => 'eg_views_handler_filter_freetext',
    ),
  );

  $data[$table_name]['EGssa'] = array(
    'title' => 'Subject area',
    'help' => 'Academic subject area the resource is relevant to',
    'field' => array(
      'handler' => 'eg_views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'eg_views_handler_filter_facet',
    ),
    'sort' => array(
      'handler' => 'eg_views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'eg_views_handler_argument',
    ),
  );

  $data[$table_name]['EGgeogcoverage'] = array(
    'title' => 'Geographic area',
    'help' => 'Geographic area the resource is relevant to',
    'field' => array(
      'handler' => 'eg_views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'eg_views_handler_filter_facet',
    ),
    'sort' => array(
      'handler' => 'eg_views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'eg_views_handler_argument',
    ),
  );

  $data[$table_name]['EGpublisher'] = array(
    'title' => 'Publisher',
    'help' => 'Original publisher of this content',
    'field' => array(
      'handler' => 'eg_views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'eg_views_handler_filter_facet',
    ),
    'sort' => array(
      'handler' => 'eg_views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'eg_views_handler_argument',
    ),
  );

  $data[$table_name]['EGsupportservices'] = array(
    'title' => 'Support service',
    'help' => 'Related Excellence Gateway support service',
    'field' => array(
      'handler' => 'eg_views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'eg_views_handler_filter_facet',
    ),
    'sort' => array(
      'handler' => 'eg_views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'eg_views_handler_argument',
    ),
  );
  $data[$table_name]['EGaudience'] = array(
    'title' => 'Audience',
    'help' => 'Intended audience for the resource',
    'field' => array(
      'handler' => 'eg_views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'eg_views_handler_filter_facet',
    ),
    'sort' => array(
      'handler' => 'eg_views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'eg_views_handler_argument',
    ),
  );

  $data[$table_name]['EGinfoarch2011'] = array(
    'title' => 'IA area',
    'help' => 'Area of the Excellence Gateway information architecture',
    'field' => array(
      'handler' => 'eg_views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'eg_views_handler_filter_facet',
    ),
    'sort' => array(
      'handler' => 'eg_views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'eg_views_handler_argument',
    ),
  );

  $data[$table_name]['EGprovisionlearnerfocus'] = array(
    'title' => 'Learner focus',
    'help' => 'Provision learner focus',
    'field' => array(
      'handler' => 'eg_views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'eg_views_handler_filter_facet',
    ),
    'sort' => array(
      'handler' => 'eg_views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'eg_views_handler_argument',
    ),
  );

  $data[$table_name]['LSISplatforms'] = array(
    'title' => 'Platform',
    'help' => 'Relevant LSIS platform',
    'field' => array(
      'handler' => 'eg_views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'eg_views_handler_filter_facet',
    ),
    'sort' => array(
      'handler' => 'eg_views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'eg_views_handler_argument',
    ),
  );

  $data[$table_name]['EGproviders'] = array(
    'title' => 'Provider',
    'help' => 'Type of provider',
    'field' => array(
      'handler' => 'eg_views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'eg_views_handler_filter_facet',
    ),
    'sort' => array(
      'handler' => 'eg_views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'eg_views_handler_argument',
    ),
  );

  $data[$table_name]['SfLlevels'] = array(
    'title' => 'SfL level',
    'help' => 'Skills for life core curriculum level',
    'field' => array(
      'handler' => 'eg_views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'eg_views_handler_filter_facet',
    ),
    'sort' => array(
      'handler' => 'eg_views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'eg_views_handler_argument',
    ),
  );

  $data[$table_name]['SfLCoreCurricula'] = array(
    'title' => 'Core curriculum',
    'help' => 'Skills for Life Core Curriculum area',
    'field' => array(
      'handler' => 'eg_views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'eg_views_handler_filter_facet',
    ),
    'sort' => array(
      'handler' => 'eg_views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'eg_views_handler_argument',
    ),
  );

  $data[$table_name]['EGyearofpublication'] = array(
    'title' => 'Year of publication',
    'help' => 'Year this resource was published',
    'field' => array(
      'handler' => 'eg_views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'eg_views_handler_filter_facet',
    ),
    'sort' => array(
      'handler' => 'eg_views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'eg_views_handler_argument',
    ),
  );

  $data[$table_name]['EGcuratedcollections'] = array(
    'title' => 'Curated collection',
    'help' => 'Curated collections this resource was featured in',
    'field' => array(
      'handler' => 'eg_views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'eg_views_handler_filter_facet',
    ),
    'sort' => array(
      'handler' => 'eg_views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'eg_views_handler_argument',
    ),
  );

  $data[$table_name]['EGsourcewebsite'] = array(
    'title' => 'Source website',
    'help' => 'Original source website for this resource',
    'field' => array(
      'handler' => 'eg_views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'eg_views_handler_filter_facet',
    ),
    'sort' => array(
      'handler' => 'eg_views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'eg_views_handler_argument',
    ),
  );

  $data[$table_name]['create-date'] = array(
    'title' => 'Date created',
    'help' => 'Date this resource was added to the Excellence Gateway',
    'field' => array(
      'handler' => 'eg_views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'eg_views_handler_filter_date',
    ),
    'sort' => array(
      'handler' => 'eg_views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'eg_views_handler_argument',
    ),
  );

  $data[$table_name]['moddate'] = array(
    'title' => 'Date modified',
    'help' => 'Date this resource was last updated',
    'field' => array(
      'handler' => 'eg_views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'eg_views_handler_filter_date',
    ),
    'sort' => array(
      'handler' => 'eg_views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'eg_views_handler_argument',
    ),
  );

  // Score field.
  $data[$table_name]['score'] = array(
    'title' => t('Score'),
    'help' => t('Score'),
    'field' => array(
      'handler' => 'eg_views_handler_field',
      'click sortable' => TRUE,
    ),
  );

  $data[$table_name]['EGresourcetype'] = array(
    'title' => 'Resource Type',
    'help' => 'Excellence Gateway resource type',
    'field' => array(
      'handler' => 'eg_views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'eg_views_handler_filter_facet',
    ),
    'sort' => array(
      'handler' => 'eg_views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'eg_views_handler_argument',
    ),
  );

  $data[$table_name]['pid'] = array(
    'title' => 'PID',
    'help' => 'The Fedora ID (PID) of the resource',
    'field' => array(
      'handler' => 'eg_views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'eg_views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'eg_views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'eg_views_handler_argument',
    ),
  );

  $data[$table_name]['mime-type'] = array(
    'title' => 'Mime Type',
    'help' => 'The mime-type of the resource',
    'field' => array(
      'handler' => 'eg_views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'eg_views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'eg_views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'eg_views_handler_argument',
    ),
  );

  $data[$table_name]['score'] = array(
    'title' => 'Score',
    'help' => 'The search result score',
    'field' => array(
      'handler' => 'eg_views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'eg_views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'eg_views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'eg_views_handler_argument',
    ),
  );
  return $data;
}
